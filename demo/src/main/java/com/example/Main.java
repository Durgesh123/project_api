package com.example;
import java.util.*;

public class Main{
    public static void main(String args[]){
        System.out.println("Player Info");
        //  Application.launch(PlayersData.class,args);

        PlayersData pd=new PlayersData();
        // pd.setPlayerName("Durgesh");
        // pd.setCountry("India");
        // pd.setAge(22);

        // System.out.println("player Name : "+ pd.getPlayerName());
        // System.out.println("Country Name :"+ pd.getCountry());
        // System.out.println("player age : "+ pd.getAge());

        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter Player name :");
             String playerName = sc.nextLine();

             System.out.println("Enter Country name :");
             String countryName = sc.nextLine();

             System.out.println("Enter Age");
             int age=sc.nextInt();


             pd = new PlayersData();

             pd.setCountry(countryName);
             pd.setAge(age);
             pd.setPlayerName(playerName);
        }

         System.out.println("Player name "+pd.getPlayerName());
         System.out.println("Player country "+pd.getCountry());
         System.out.println("Player age "+pd.getAge());

    }
}