package com.firebase.controller;


import java.io.FileInputStream;
import java.io.IOException;

import com.firebase.firebase_connection.FirebaseService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
//import com.google.firebase.internal.FirebaseService;
import com.google.firebase.auth.FirebaseAuthException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginController extends Application {

    private Stage primaryStage;
    private FirebaseService firebaseService;
    private Scene scene;
    public void setPrimaryStageScene(Scene scene){
        primaryStage.setScene(scene);
       
    }
    public void initializeLoginScene(){
        Scene loginScene = createLoginScene();
        this.scene = loginScene;
        primaryStage.setScene(loginScene);
    }

      public void start (Stage primaryStage){
        this.primaryStage = primaryStage;

        try{
            FileInputStream serviceAccount = new FileInputStream("firebase\\src\\main\\resources\\fx.json");
           
            FirebaseOptions options = new FirebaseOptions.Builder()
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
            .setDatabaseUrl("https://fir-3bad7-default-rtdb.asia-southeast1.firebasedatabase.app/")
            .build();
            FirebaseApp.initializeApp(options);
        }catch(IOException e){
            e.printStackTrace();
        }
        Scene scene = createLoginScene();
        this.scene = scene;
        primaryStage.setTitle("firebase auth example");
        primaryStage.setScene(scene);
        primaryStage.show();
      }
      private Scene createLoginScene(){
        TextField emailField = new TextField();
        emailField.setPromptText("Email");

        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("password");

        Button signUpButton = new Button("sign up");
        Button loginButton = new Button("log in");
       firebaseService = new FirebaseService(this, emailField ,passwordField);

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                try {
                    firebaseService.signUp();
                } catch (FirebaseAuthException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event){
                firebaseService.login();
            }
        });
        VBox fieldBox = new VBox(20,emailField,passwordField);
        HBox buttonBox = new HBox(20,loginButton,signUpButton);
        VBox comBox = new  VBox(10,fieldBox,buttonBox);
        Pane viewPane = new Pane(comBox);
        return new Scene(viewPane,400,200);
      }
    
}
