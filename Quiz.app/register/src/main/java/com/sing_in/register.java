package com.sing_in;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class register extends Application {
    // private static Firestore db;

    // public static void initializeFirestore() throws IOException {
    //     System.out.println("Initializing Firestore...");
    //     try (FileInputStream serviceAccount = new FileInputStream("register\\src\\main\\resources\\-fx-data.json")) {
    //         FirestoreOptions firestoreOptions = FirestoreOptions.getDefaultInstance().toBuilder()
    //                 .setProjectId("qizeapp-c932e")
    //                 .setCredentials(GoogleCredentials.fromStream(serviceAccount))
    //                 .build();
    //         db = firestoreOptions.getService();
    //         System.out.println("Firestore initialized successfully.");
    //     }
    // }

    @Override
    public void start(Stage prStage) {
        // Sign in Label
        Label sign = new Label("Sign in");
        sign.setStyle("-fx-text-fill: WHITE;");
        sign.setPadding(new Insets(20));
        sign.setFont(Font.font("Arial", FontWeight.BOLD, 35));

       

        Label userLabel = new Label("Usre Name");
        userLabel.setPadding(new Insets(7, 200, 5, 0));
        userLabel.setStyle("-fx-text-fill:WHITE;");
        userLabel.setFont(Font.font("Arial", 12));

        TextField userField = new TextField();
        userField.setMaxWidth(250);

        // Textfield Email
        TextField emailField = new TextField();
        emailField.setMaxWidth(250);

        // Email label
        Label emaiLabel = new Label("Email");
        emaiLabel.setPadding(new Insets(15, 205, 5, 0));
        emaiLabel.setStyle("-fx-text-fill:WHITE;");
        emaiLabel.setFont(Font.font("Arial", 12));

        // Textfield pass
        PasswordField passField = new PasswordField();
        passField.setMaxWidth(250);
    

        Label passLabel = new Label("Password");
        passLabel.setPadding(new Insets(20, 195, 7, 0));
        passLabel.setStyle("-fx-text-fill:WHITE;");
        passLabel.setFont(Font.font("Arial", 12));
        
        // error lable
        Label errorLa=new Label();
        errorLa.setStyle("-fx-text-fill:red;");
        errorLa.setPadding(new Insets(20,0,0,0));

        // Login Button
        Button LoginBt = new Button("Sign Up");
        LoginBt.setLayoutX(630);
        LoginBt.setLayoutY(370);
        LoginBt.setStyle(
                "-fx-background-color:RED; -fx-text-fill: WHITE; -fx-font-size: 18px; -fx-min-width: 130px; -fx-min-height: 40px;-fx-background-radius:20;");
        LoginBt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                 String email = emailField.getText();
                 String password = passField.getText();
                 String  username = userField.getText();
                  if (!email.isEmpty() && !password.isEmpty() && !username.isEmpty() ) {
                     try {
                //         saveUserData(email, password, username);
                    //    Profile  nx=new Profile(email,username);
                    //     nx.start(prStage);
                   } catch (Exception ioException) {
                        ioException.printStackTrace();
                    }
                 }else {
                  
                       errorLa.setText("Please enter both email and password.");
                       errorLa.setPadding(new Insets(0,0,10,0));
                 }
                
               
            }
        });
        LoginBt.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                LoginBt.setStyle(
                        "-fx-background-color: ORANGE; -fx-text-fill: WHITE; -fx-font-size: 18px; -fx-min-width: 130px; -fx-min-height: 40px;-fx-background-radius:20;");
            }
        });
        LoginBt.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                LoginBt.setStyle(
                        "-fx-background-color: RED; -fx-text-fill: WHITE; -fx-font-size: 18px; -fx-min-width: 130px; -fx-min-height: 40px;-fx-background-radius:20;");
            }
        });

        // Back Button
        Button backButton = new Button("<-");
        backButton.setStyle(
                "-fx-min-width:60px;-fx-min-height:30px;-fx-background-radius:20;-fx-background-color:BLACK;-fx-text-fill:WHITE");
        backButton.setLayoutX(10);
        backButton.setLayoutY(10);
        backButton.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                backButton.setStyle(
                        "-fx-background-color:RED;-fx-min-width:60px;-fx-min-height:30px;-fx-background-radius:20;");
            }
        });
        backButton.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                backButton.setStyle(
                        "-fx-min-width:60px;-fx-min-height:30px;-fx-background-radius:20;-fx-background-color:BLACK;-fx-text-fill:WHITE");
            }
        });

        // Google image
        Image googlimg = new Image("image/google.png");
        ImageView googlview = new ImageView(googlimg);
        googlview.setFitWidth(30);
        googlview.setFitHeight(30);
        googlview.setLayoutX(580);
        googlview.setLayoutY(470);

        Image facelimg = new Image("image/facebook.png");
        ImageView facelview = new ImageView(facelimg);
        facelview.setFitWidth(35);
        facelview.setFitHeight(35);
        facelview.setLayoutX(680);
        facelview.setLayoutY(470);

        Image gitlimg = new Image("image/github.png");
        ImageView gitview = new ImageView(gitlimg);
        gitview.setFitWidth(40);
        gitview.setFitHeight(40);
        gitview.setLayoutX(770);
        gitview.setLayoutY(470);

        VBox vb = new VBox(sign,userLabel,userField, emaiLabel,emailField , passLabel, passField,errorLa);
        vb.setAlignment(Pos.TOP_CENTER);
        vb.setMinSize(400, 500);
        vb.setLayoutX(500);
        vb.setLayoutY(70);
        vb.setStyle("-fx-background-color:BLACK;-fx-background-radius: 20;");
        Group gp = new Group( vb,backButton, googlview, facelview, gitview,LoginBt);
        Scene sc = new Scene(gp, 1350, 700);
        sc.setFill(Color.web("#508BFC"));
        prStage.setScene(sc);
        prStage.setResizable(false);
        prStage.show();
    }

    // public void saveUserData(String email, String password,String username) throws IOException {
    //     if (db == null) {
    //         initializeFirestore();
    //     }

    //     Map<String, Object> user = new HashMap<>();
    //     user.put("username", email);
    //     user.put("password", password);
    //     user.put("password", username);

    //     DocumentReference docRef = db.collection("users").document();
    //     try {
    //         docRef.set(user).get();
    //         System.out.println("User added with ID: " + docRef.getId());
    //     } catch (InterruptedException | ExecutionException e) {
    //         System.err.println("Error adding user: " + e.getMessage());
    //     }
    // }

}
