package com.sing_in;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Profile extends Application {

    private Label nameLabel;
    private Label contactLabel;
    private Label collegeLabel;
    private Label dobLabel;
    private Label aboutLabel;
    private String Email;
    private String User;

    // public Profile(String email,String user){
    // this.Email = email;
    // this.User = user;
    // }

    // private static Firestore db;

    // public static void initializeFirestore() throws IOException {
    // System.out.println("Initializing Firestore...");
    // try (FileInputStream serviceAccount = new
    // FileInputStream("registe/src/main/resources/-fx-data.json")) {
    // FirestoreOptions firestoreOptions =
    // FirestoreOptions.getDefaultInstance().toBuilder()
    // .setProjectId("qizeapp-c932e")
    // .setCredentials(GoogleCredentials.fromStream(serviceAccount))
    // .build();
    // db = firestoreOptions.getService();
    // System.out.println("Firestore initialized successfully.");
    // }
    // }

    @Override
    public void start(Stage prStage) {
        String imagePath = "image/boy.png";
        Image image = new Image(imagePath);
        Circle obj = new Circle(70);
        obj.setFill(new ImagePattern(image));

        Image img = new Image("image/pencil.png");
        ImageView imgv = new ImageView(img);
        imgv.setFitHeight(20);
        imgv.setFitWidth(20);

        Button editButton = new Button(null, imgv);
        editButton.setLayoutX(450);
        editButton.setLayoutY(250);
        editButton.setStyle("-fx-background-color:transparent");

        editButton.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif"));
            File selectedFile = fileChooser.showOpenDialog(prStage);
            if (selectedFile != null) {
                Image newImage = new Image(selectedFile.toURI().toString());
                obj.setFill(new ImagePattern(newImage));
            }
        });

        Image inta = new Image("/image/instagram.png");
        ImageView instView = new ImageView(inta);
        instView.setFitHeight(50);
        instView.setFitWidth(50);
        instView.setLayoutX(270);
        instView.setLayoutY(370);

        Image twit = new Image("/image/social.png");
        ImageView twitView = new ImageView(twit);
        twitView.setFitHeight(50);
        twitView.setFitWidth(50);
        twitView.setLayoutX(370);
        twitView.setLayoutY(370);

        Image link = new Image("/image/linkedin.png");
        ImageView linkView = new ImageView(link);
        linkView.setFitHeight(50);
        linkView.setFitWidth(50);
        linkView.setLayoutX(470);
        linkView.setLayoutY(370);

        // Label lb = new Label(User);
        // lb.setStyle("-fx-text-fill: black; -fx-font-size: 15; -fx-font-weight:
        // bold;");

        nameLabel = new Label("Name: ");
        nameLabel.setStyle("-fx-text-fill:black; -fx-font-size: 15; -fx-font-weight: bold");
        nameLabel.setPadding(new Insets(6, 0, 0, 20));
        contactLabel = new Label("Contact: ");
        contactLabel.setStyle("-fx-text-fill: black; -fx-font-size: 15; -fx-font-weight: bold");
        contactLabel.setPadding(new Insets(0, 0, 0, 20));
        collegeLabel = new Label("College: ");
        collegeLabel.setStyle("-fx-text-fill: black; -fx-font-size: 15; -fx-font-weight: bold");
        collegeLabel.setPadding(new Insets(0, 0, 0, 20));
        dobLabel = new Label("DOB: ");
        dobLabel.setStyle("-fx-text-fill: black; -fx-font-size: 15; -fx-font-weight: bold");
        dobLabel.setPadding(new Insets(0, 0, 0, 20));
        aboutLabel = new Label("About: ");
        aboutLabel.setStyle("-fx-text-fill: black; -fx-font-size: 15; -fx-font-weight: bold");
        aboutLabel.setPadding(new Insets(0, 0, 5, 20));
        Label email = new Label("Email :" + Email);
        email.setStyle("-fx-text-fill: black; -fx-font-size: 15; -fx-font-weight: bold");
        email.setPadding(new Insets(0, 0, 0, 20));

        Label errorLa = new Label();
        errorLa.setStyle("-fx-text-fill:red;");
        errorLa.setPadding(new Insets(20, 0, 0, 0));

        Button showPopupButton = new Button("Edit");
        showPopupButton.setLayoutX(1000);
        showPopupButton.setLayoutY(470);
        ;
        showPopupButton.setStyle(
                "-fx-background-color:blue; -fx-text-fill: WHITE; -fx-font-size: 18px; -fx-min-width: 130px; -fx-min-height: 40px;-fx-background-radius:20;");
        showPopupButton.setOnAction(event -> showPopup(prStage));

        VBox infoBox = new VBox(20, nameLabel, email, contactLabel, collegeLabel, dobLabel, aboutLabel);
        infoBox.setMinSize(550, 280);
        infoBox.setStyle("-fx-background-color: white; -fx-background-radius: 20;");
        infoBox.setLayoutX(600);
        infoBox.setLayoutY(150);
        infoBox.setAlignment(Pos.TOP_LEFT);

        VBox vbox = new VBox(10, obj);
        vbox.setMinSize(320, 300);
        vbox.setLayoutX(240);
        vbox.setLayoutY(150);
        vbox.setStyle("-fx-background-color: white; -fx-background-radius: 20;");
        vbox.setAlignment(Pos.TOP_CENTER);

        Image scenImage = new Image("/image/back1.jpg");
        ImageView img1 = new ImageView(scenImage);
        img1.setFitHeight(700);
        img1.setFitWidth(1350);

        StackPane root = new StackPane(img1);

        Group gp = new Group(root, vbox, editButton, imgv, instView, twitView, linkView, infoBox, showPopupButton);
        Scene sc = new Scene(gp, 1350, 700);
        sc.setFill(Color.web("#F5F5DC"));
        prStage.setResizable(false);
        prStage.setScene(sc);
        prStage.show();
    }

    private void showPopup(Stage parentStage) {
        Stage popup = new Stage();
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.initOwner(parentStage);

        VBox popupVBox = new VBox(10);

        TextField nameField = new TextField();
        TextField contactField = new TextField();
        TextField collegeField = new TextField();
        TextField dobField = new TextField();
        TextField aboutField = new TextField();
        Button setValueButton = new Button("Save");

        setValueButton.setOnAction(event -> {
            nameLabel.setText("Name: " + nameField.getText());
            contactLabel.setText("Contact: " + contactField.getText());
            collegeLabel.setText("College: " + collegeField.getText());
            dobLabel.setText("DOB: " + dobField.getText());
            aboutLabel.setText("About: " + aboutField.getText());

            String name = nameField.getText();
            String contact = contactField.getText();
            String college = collegeField.getText();
            String dob = dobField.getText();
            String about = aboutField.getText();
            if (!name.isEmpty() && !contact.isEmpty() && !college.isEmpty() && !dob.isEmpty() && !about.isEmpty()) {
                try {
                    // saveUserData(name,contact,college,dob,about);
                    // Nextpage nx=new Nextpage(email,username);
                    // nx.start(prStage);
                } catch (Exception ioException) {
                    ioException.printStackTrace();
                }
            }
            popup.close();
        });

        popupVBox.getChildren().addAll(

                new Label("Name:"), nameField,
                new Label("Contact:"), contactField,
                new Label("College:"), collegeField,
                new Label("DOB:"), dobField,
                new Label("About:"), aboutField,
                setValueButton

        );
        popupVBox.setAlignment(Pos.TOP_CENTER);
        popupVBox.setStyle("-fx-padding: 20;");

        Scene popupScene = new Scene(popupVBox, 300, 400);
        popup.setScene(popupScene);
        popup.showAndWait();
    }

    private void saveToFile(String text) {
        try (FileWriter writer = new FileWriter("saved_text.txt", true)) {
            writer.write(text + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // public void saveUserData(String name, String contact,String college, String
    // dob, String about) throws IOException {
    // if (db == null) {
    // initializeFirestore();
    // }

    // Map<String, Object> Profile = new HashMap<>();
    // Profile.put("About", about);
    // Profile.put("DOB", dob);
    // Profile.put("College", college);
    // Profile.put("Contact", contact);
    // Profile.put("Name", name);

    // DocumentReference docRef = db.collection("Profile").document();
    // try {
    // docRef.set(Profile).get();
    // System.out.println("User added with ID: " + docRef.getId());
    // } catch (InterruptedException | ExecutionException e) {
    // System.err.println("Error adding user: " + e.getMessage());
    // }
    // }

}
