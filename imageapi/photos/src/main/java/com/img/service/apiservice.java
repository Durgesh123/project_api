package com.img.service;
import java.io.IOException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.json.JSONObject;
import netscape.javascript.JSObject;

public class apiservice extends Application {
    
    static String imgURL="";

    public static void imageData()throws IOException{
        StringBuffer response =new dataurl().getResponceData();
        if(response != null){
           JSONObject obj = new JSONObject(response.toString());
           JSONObject urlobj =obj.getJSONObject("urls");

           imgURL = urlobj.getString("small");
        }else{
            System.out.println("Responce is empty");
        }
    }
    @Override
    public void start(Stage prStage)throws Exception{
          imageData();

          Image img = new Image(imgURL);

          ImageView imageView = new ImageView(img);

          Pane imgpane = new Pane();
          imgpane.getChildren().add(imageView);

          Scene sc=new Scene(imgpane,img.getWidth(),img.getHeight());
          prStage.setScene(sc);

          prStage.setTitle("Image");
          prStage.show();
    }
}
