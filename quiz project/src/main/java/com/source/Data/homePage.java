package com.source.Data;

import com.google.errorprone.annotations.OverridingMethodsMustInvokeSuper;
import com.source.controller.FromNavigationApp;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import javafx.scene.text.Font;

import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class homePage {
 private FromNavigationApp app;
     private GridPane view;
     private TextField field2=new TextField();
     
    public homePage(FromNavigationApp app) {

        this.app = app;
        initialize();
      }
    
      private void initialize() {

        view = new GridPane();
        view.setPadding(new Insets(1,1,1,1));

        Image bcImage = new Image("languages.jpeg");
        ImageView bcIv = new ImageView(bcImage);
        bcIv.setFitHeight(1080);
        bcIv.setFitWidth(1920);
        bcIv.setPreserveRatio(true);
        bcIv.setOpacity(0.7);

        //Label label = new Label("Hello");

        Text text2 = new Text("Quizzo!");
        //text2.setFont(new Font(170)); // Set the font size
        text2.getStyleClass().add("text-style"); // Set the tex
        // Position the text at the desired location
        text2.setX(150); // X position
        text2.setY(60); // Y position   


        Button button1 = new Button("<- Back");
        button1.setLayoutX(10);
        button1.setLayoutY(10);
        button1.setMinWidth(30);
        button1.setMinHeight(10);
        button1.setFont(new Font(25));;
       // button1.setStyle("-fx-border-radius:50;"+" -fx-background-radius: 50;"+"-fx-border-Color:purple;"+"-fx-background-color:purple;"+"-fx-text-fill: white;");
       button1.getStyleClass().add("button-unique-back");
      button1.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event){

          app.navigateToPage3();
        }
      });

         Image image = new Image("homeButton.png"); // Make sure the path is correct

        // Create an ImageView
        ImageView imageView = new ImageView(image);

        // Optionally set the size of the ImageView
        imageView.setFitWidth(56);
        imageView.setFitHeight(56);

        // Create a Button and set the ImageView as its graphic
        Button button2 = new Button();
        button2.setGraphic(imageView);
        button2.setStyle("-fx-background-color:#CBC3E3;"+"-fx-text-fill: white;");
        button2.setOnMouseEntered(e -> button2.setStyle("-fx-background-color: red;"));
        button2.setOnMouseExited(e -> button2.setStyle("-fx-background-color: pink;"));
        button2.setOnAction(new EventHandler<ActionEvent>() {
          
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage3();
          }
        });


        Image image3 = new Image("profile.jpg"); // Make sure the path is correct

        // Create an ImageView
        ImageView imageView3 = new ImageView(image3);

        // Optionally set the size of the ImageView
        imageView3.setFitWidth(56);
        imageView3.setFitHeight(56);

        // Create a Button and set the ImageView as its graphic
        Button button4 = new Button();
        button4.setGraphic(imageView3);
        button4.setStyle("-fx-background-color:#CBC3E3;"+"-fx-text-fill: white;");
        button4.setOnMouseEntered(e -> button4.setStyle("-fx-background-color: red;"));
        button4.setOnMouseExited(e -> button4.setStyle("-fx-background-color: pink;"));
        button4.setOnAction(new EventHandler<ActionEvent>() {
          
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage6();
          }
        });


        HBox hBox1 = new HBox(button2,button4);
        hBox1.setMinWidth(1600);
        hBox1.setMinHeight(60);
        
        hBox1.setLayoutY(910);
        hBox1.setLayoutX(110);
        hBox1.setMargin(button2, new Insets(10, 0, 0, 180)); 
        hBox1.setMargin(button4, new Insets(10, 0, 0, 1100)); 
        hBox1.getStyleClass().add("hbox-footer");
        Image image2 = new Image("searchIcon.png"); // Make sure the path is correct

        // Create an ImageView
        ImageView imageView2 = new ImageView(image2);

        // Optionally set the size of the ImageView
        imageView2.setFitWidth(40);
        imageView2.setFitHeight(40);

          TextField searchField = new TextField();
        searchField.setPromptText("Search...");
        searchField.setMinWidth(300);
        searchField.setMinHeight(50);
        searchField.setStyle("-fx-background-radius:50");

        Button searchButton = new Button();
        searchButton.setGraphic(imageView2);
        searchButton.setOnAction(e -> {
            String query = searchField.getText();
            System.out.println("Searching for: " + query);
        });

        // Create an HBox to hold the search field and button
        HBox searchBox = new HBox(5, searchField, searchButton);
        searchBox.setPadding(new Insets(10));
        searchBox.setLayoutX(1500);

       Text text4 = new Text("Daily Quizz  \nLearn and Solve");
        text4.setFont(Font.font("Arial", FontWeight.BOLD, 40));
       
        DropShadow shadow = new DropShadow();
        shadow.setOffsetX(3.0);
        shadow.setOffsetY(3.0);
        shadow.setColor(Color.GRAY);
        text4.setEffect(shadow);
        text4.setLayoutX(750);
        text4.setLayoutY(150);
        text4.getStyleClass().add("button-unique-text");
        text4.setFill(Color.WHEAT);

    
        Label QQ = new Label("10 Quiz");
        QQ.setStyle("-fx-font-size: 22px;");
        Label QQ2 = new Label("10 Quiz");
        QQ2.setStyle("-fx-font-size: 22px;");
        Label QQ3 = new Label("10 Quiz");
        QQ3.setStyle("-fx-font-size: 22px;");
        Label QQ4 = new Label("10 Quiz");
        QQ4.setStyle("-fx-font-size: 22px;");
        Label QQ5 = new Label("10 Quiz");
        QQ5.setStyle("-fx-font-size: 22px;");
        Label QQ6 = new Label("10 Quiz");
        QQ6.setStyle("-fx-font-size: 22px;");


        Button bt1 = new Button("Start");
        bt1.getStyleClass().add("bt-st");
        Label lb1 = new Label("C++ Quiz");
        lb1.getStyleClass().add("lable");
        Image img1 = new Image("CPP.png");
        ImageView iv1 = new ImageView(img1);
        iv1.setFitWidth(80);
        iv1.setFitHeight(80);
        HBox quiz1 = new HBox(iv1,lb1,QQ2,bt1);
        quiz1.setMinWidth(600);
        quiz1.setMinHeight(150);
        quiz1.setStyle("-fx-background-radius:30");
        quiz1.setLayoutX(230);
        quiz1.setLayoutY(300);
        quiz1.getStyleClass().add("quiz-style");
        quiz1.setMargin(iv1, new Insets(30, 0, 0, 10)); 
        quiz1.setMargin(lb1, new Insets(27, 0, 0, 30)); 
        quiz1.setMargin(QQ2, new Insets(100, 0, 0, 60));
        quiz1.setMargin(bt1, new Insets(40, 0, 0, 50));
        
        
        Button bt2 = new Button("Start");
        bt2.getStyleClass().add("bt-st");
        bt2.setOnAction(new EventHandler<ActionEvent>() {
          
          @Override
          public void handle(ActionEvent event){

            app.navigateToPage4();
          }
        });
        Label lb2 = new Label("Java Quiz");
        lb2.getStyleClass().add("lable");
        Image img2 = new Image("Java.jpeg");
        ImageView iv2 = new ImageView(img2);
        iv2.setFitWidth(80);
        iv2.setFitHeight(80);
        HBox quiz2 = new HBox(iv2,lb2,QQ3,bt2);
        quiz2.setMinWidth(600);
        quiz2.setMinHeight(150);
        quiz2.setStyle("-fx-background-radius:30");
        quiz2.setLayoutX(930);
        quiz2.setLayoutY(300);
        quiz2.getStyleClass().add("quiz-style");
        quiz2.setMargin(iv2, new Insets(30, 0, 0, 10));
        quiz2.setMargin(lb2, new Insets(27, 0, 0, 30));  
        quiz2.setMargin(QQ3, new Insets(100, 0, 0, 55));
        quiz2.setMargin(bt2, new Insets(40, 0, 0, 50));


        Button bt3 = new Button("Start");
        bt3.getStyleClass().add("bt-st");
        Label lb3 = new Label("Python Quiz");
        lb3.getStyleClass().add("lable");
        Image img3 = new Image("python.jpeg");
        ImageView iv3 = new ImageView(img3);
        iv3.setFitWidth(80);
        iv3.setFitHeight(80);
        HBox quiz3 = new HBox(iv3,lb3,QQ4,bt3);
        quiz3.setMinWidth(600);
        quiz3.setMinHeight(150);
        quiz3.setStyle("-fx-background-radius:30");
        quiz3.setLayoutX(230);
        quiz3.setLayoutY(500);
        quiz3.getStyleClass().add("quiz-style");
        quiz3.setMargin(iv3, new Insets(30, 0, 0, 10)); 
        quiz3.setMargin(lb3, new Insets(27, 0, 0, 30)); 
        quiz3.setMargin(QQ4, new Insets(100, 00, 0, 10));
        quiz3.setMargin(bt3, new Insets(40, 0, 0, 50));


        Button bt4 = new Button("Start");
        bt4.getStyleClass().add("bt-st");
        Label lb4 = new Label("NT Quiz");
        lb4.getStyleClass().add("lable");
        Image img4 = new Image("networking.jpg");
        ImageView iv4 = new ImageView(img4);
        iv4.setFitWidth(80);
        iv4.setFitHeight(80);
        HBox quiz4 = new HBox(iv4,lb4,QQ5,bt4);
        quiz4.setMinWidth(600);
        quiz4.setMinHeight(150);
        quiz4.setStyle("-fx-background-radius:30");
        quiz4.setLayoutX(930);
        quiz4.setLayoutY(500);
        quiz4.getStyleClass().add("quiz-style");
        quiz4.setMargin(iv4, new Insets(30, 0, 0, 10)); 
        quiz4.setMargin(lb4, new Insets(27, 0, 0, 30));
        quiz4.setMargin(QQ5, new Insets(100, 0, 0, 90)); 
        quiz4.setMargin(bt4, new Insets(40, 0, 0, 50));


        Button bt5 = new Button("Start");
        bt5.getStyleClass().add("bt-st");
        Label lb5 = new Label("OS Quiz");
        lb5.getStyleClass().add("lable");
        Image img5 = new Image("operating-sytem.jpg");
        ImageView iv5 = new ImageView(img5);
        iv5.setFitWidth(80);
        iv5.setFitHeight(80);
        HBox quiz5 = new HBox(iv5,lb5,QQ6,bt5);
        quiz5.setMinWidth(600);
        quiz5.setMinHeight(150);
        quiz5.setStyle("-fx-background-radius:30");
        quiz5.setLayoutX(230);
        quiz5.setLayoutY(700);
        quiz5.getStyleClass().add("quiz-style");
        quiz5.setMargin(iv5, new Insets(30, 0, 0, 10)); 
        quiz5.setMargin(lb5, new Insets(27, 0, 0, 30)); 
        quiz5.setMargin(QQ6, new Insets(100, 00, 0, 80));
        quiz5.setMargin(bt5, new Insets(40, 0, 0, 50));


        Button bt6 = new Button("Start");
        bt6.getStyleClass().add("bt-st");
        Label lb6 = new Label("DBMS Quiz");
        lb6.getStyleClass().add("lable");
        Image img6 = new Image("DBMS.jpg");
        ImageView iv6 = new ImageView(img6);
        iv6.setFitWidth(80);
        iv6.setFitHeight(80);
        HBox quiz6 = new HBox(iv6,lb6,QQ,bt6);
        quiz6.setMinWidth(600);
        quiz6.setMinHeight(150);
        quiz6.setStyle("-fx-background-radius:30");
        quiz6.setLayoutX(930);
        quiz6.setLayoutY(700);
        quiz6.getStyleClass().add("quiz-style");
        quiz6.setMargin(iv6, new Insets(30, 0, 0, 10)); 
        quiz6.setMargin(lb6, new Insets(27, 0, 0, 30)); 
        quiz6.setMargin(QQ, new Insets(100, 0, 0, 60)); 
        quiz6 .setMargin(bt6, new Insets(40, 0, 0, 25));

        StackPane stp = new StackPane(bcIv);

        Group gp = new Group(stp,button1,hBox1,searchBox,quiz1,quiz2,quiz3,quiz4,quiz5,quiz6,text2,text4);

        view.add(gp,1,1);
      //   Scene sc = new Scene(gp,1920,1080);
      //   sc.getStylesheets().add(getClass().getResource("style2.css").toExternalForm()); 
      //   primaryStage.setScene(sc);
      //  //sc.setFill(Color.SKYBLUE);
      //   primaryStage.show();

    }
    public GridPane getView() {

      return view;
    }
  
    public String getField2Value() {
  
      return "welcome";//field2.getText();
    }
  
    public void setField2Value(String value) {
  
      field2.setText("welcome");
    }

}
