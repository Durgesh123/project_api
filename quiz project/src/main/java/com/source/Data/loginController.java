package com.source.Data;


import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.source.controller.FromNavigationApp;

import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class loginController {

    private TextField emailField;
    private  PasswordField passwordField;
    private register app;
    public FromNavigationApp frm;

    public loginController(FromNavigationApp frm,register app, TextField emailField, PasswordField passwordField) {
        this.app = app;
        this.emailField = emailField;
        this.passwordField = passwordField;
        this.frm=frm;
    }

    public boolean signUp() {
        String email = emailField.getText();
        String password = passwordField.getText();

        try {
            UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail(email)
                .setPassword(password)
                .setDisabled(false);

            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully signed up: " + userRecord.getUid());
            showAlert("Success", "User created successfully");
            return true;
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            showAlert("Error", "Failed to create user: " + e.getMessage());
            return false;
        }
    }

    public  boolean login() {
        String email = emailField.getText();
        String password = passwordField.getText();

        try {
            URL url = new URL("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDYI3ReL7Zw9cQP7pav1mkcj_WyHtbz7ho");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("email", email);
            jsonRequest.put("password", password);
            jsonRequest.put("returnSecureToken", true);

            try (OutputStream os = conn.getOutputStream()) {
                byte[] input = jsonRequest.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            if (conn.getResponseCode() == 200) {
                showAlert("Success", "Login successful");
               System.out.println("Welcome");
                frm.navigateToPage3();
                return true;
               
            } else {
                showAlert("Invalid Login", "Invalid credentials");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            showAlert("Error", "Failed to login: " + e.getMessage());
            return false;
        }
    }

    private static void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
