package com.source.Data;

import com.source.controller.FromNavigationApp;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class aboutUs {

     private FromNavigationApp app;
     private GridPane view;
     private TextField field7;
     
    public aboutUs(FromNavigationApp app) {

        this.app = app;
        initialize();
      }
       // Array of image paths
    private final String[] imagePaths = {
      "image12.jpg",
      "image27.jpg",
      "maxresdefault.jpg",
      "core2web.jpeg"
 };

 // ImageView to display images
 private final ImageView iV = new ImageView();

 // Current index of the image being displayed
 private int currentIndex = 0;
    
      private void initialize() {

        updateImage();

        view = new GridPane();
        view.setPadding(new Insets(1,1,1,1));

         // Home Button
        Button home = new Button("Home");
        //home.setMaxSize(100, 40);
        home.getStyleClass().add("button-style");
        home.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage1();
          }
        });

        // Contact Button
        Button contact = new Button("Contact");
        //contact.setMaxSize(150, 40);
        contact.getStyleClass().add("button-style");
        contact.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event){
            app.navigateToPage8();
          }
        });


        // About Us Label
        Label aboutLabel = new Label("QUIZZO..");
        aboutLabel.getStyleClass().add("text-style");
        aboutLabel.setFont(new Font(35));
        aboutLabel.setStyle("-fx-font-weight: bold; -fx-text-fill: black;");

        // Mission Text
        Text missionText = new Text("To deliver high-quality software that meets the diverse\nneeds of our clients and empowers them to achieve\ntheir goals with ease.");
        missionText.setFont(new Font(20));

        // Team Name Label
        Label teamName = new Label("Team Prefix");
        teamName.setFont(new Font(45));
        teamName.getStyleClass().add("label");

        Text text2 = createStyledText("Quizzo!", "Arial", FontWeight.BOLD, FontPosture.ITALIC,50,50,60);
        text2.setLayoutX(50);
        text2.setLayoutY(50);

        HBox hboxButtons = new HBox(35,text2, home, contact);
        hboxButtons.setMinHeight(60);
        hboxButtons.setMinWidth(1920);
        hboxButtons.setStyle("-fx-background-color :#90EE90;");
        hboxButtons.setMargin(text2, new Insets(5, 0, 0, 30));
        hboxButtons.setMargin(home, new Insets(5, 0, 0, 1320));
        hboxButtons.setMargin(contact, new Insets(5, 0, 0, 40));

        Label sir1 = new Label("Inspiration");
        sir1.setStyle("-fx-font-size: 50px; -fx-background-color: transparent; -fx-text-fill: Purple; -fx-border-radius: 0; -fx-background-radius: 0; -fx-font-weight: bold;");
        sir1.setLayoutX(820);
        sir1.setLayoutY(450);

        Label sir = new Label("Shashi Sir");
        sir.setStyle("-fx-font-size: 50px; -fx-background-color: transparent; -fx-text-fill: Purple; -fx-border-radius: 0; -fx-background-radius: 0; -fx-font-weight: bold;");
        sir.setLayoutX(820);
        sir.setLayoutY(530);

        // Define image and circle
        Image image = new Image("sir.png");
        Circle clip = new Circle(1000);

        // Create ImageView and set clip
        ImageView imageView = new ImageView(image);
        imageView.setClip(clip);
        imageView.setFitWidth(500);
        imageView.setFitHeight(500);
        imageView.setLayoutX(700);
        imageView.setLayoutY(0);

        // Layouts
        VBox vbox = new VBox(20, hboxButtons, teamName);
        vbox.setStyle("-fx-background-color: #90EE90;");
        vbox.setMinHeight(400);
        vbox.setMaxWidth(1920);
        vbox.setLayoutX(0);
        vbox.setMargin(teamName, new Insets(200, 0, 0, 30));
        vbox.setMargin(missionText, new Insets(50, 0, 0, 30));

        Text tx1 = new Text("Thanks To All Sir :-");
        tx1.setLayoutX(780);
        tx1.setLayoutY(800);
        tx1.setStyle("-fx-font-size: 30px;-fx-font-weight: bold;");

        Text tx2 = new Text("Subhodh Dada");
        tx2.setLayoutX(500);
        tx2.setLayoutY(900);
        tx2.setStyle("-fx-font-size: 30px;");

        Text tx3 = new Text("Shiv Dada");
        tx3.setLayoutX(800);
        tx3.setLayoutY(900);
        tx3.setStyle("-fx-font-size: 30px;");
        
        Text tx4 = new Text("Rahul Dada");
        tx4.setLayoutX(1100);
        tx4.setLayoutY(900);
        tx4.setStyle("-fx-font-size: 30px;");

        Text tx5 = new Text("Sachin Sir");
        tx5.setLayoutX(700);
        tx5.setLayoutY(850);
        tx5.setStyle("-fx-font-size: 30px;");

        Text tx6 = new Text("Pramod Sir");
        tx6.setLayoutX(1000);
        tx6.setLayoutY(850);
        tx6.setStyle("-fx-font-size: 30px;");
        
        Text t1 = new Text("We are deeply grateful to our class teacher, Sachin Sir , Pramod Sir, and our mentors, Subhod Dada, \nShiv Dada, and Rahul Dada, for their invaluable guidance and support throughout this project.\n Your expertise and encouragement have been instrumental in our success. \nThank you for believing in us and helping us achieve our goals.");
        t1.setLayoutX(20);
        t1.setLayoutY(800);
        t1.setStyle("-fx-font-size: 27px;");

        VBox mainLayout = new VBox(20, vbox);
        mainLayout.setPadding(new javafx.geometry.Insets(20));

        StackPane root = new StackPane(iV);
        root.setLayoutX(1200);
        root.setLayoutY(500);

        Group gp = new Group(root,mainLayout,vbox,imageView,sir,sir1,t1);
        view.add(gp,0,0);

        // Scene scene = new Scene(gp,1920,1080);
        // primaryStage.setScene(scene);
        // primaryStage.show();

        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(2), e -> nextImage()));
        timeline.setCycleCount(Timeline.INDEFINITE); // Run indefinitely
        timeline.play();

    }
    private void updateImage() {
      Image image = new Image(imagePaths[currentIndex]);
      iV.setImage(image);
      iV.setFitWidth(700); // Set the width of the image
      iV.setFitHeight(500); // Set the height of the image
      iV.setPreserveRatio(true); // Preserve the aspect ratio of the image
  }

  private void nextImage() {
      currentIndex = (currentIndex + 1) % imagePaths.length; // Move to the next image
      updateImage();
  }

    public GridPane getView() {

        return view;
      }
    
      public String getField7Value() {
    
        return field7.getText();
      }
    
      public void setField7Value(String value) {
    
        field7.setText(value);
      }
      private Text createStyledText(String content, String fontFamily, FontWeight fontWeight, FontPosture fontPosture,
                                  double fontSize, double x, double y) {
        Text text = new Text(content);
        text.setFont(Font.font(fontFamily, fontWeight, fontPosture, fontSize));
        text.setFill(Color.BLACK);
        text.setX(1500);
        text.setY(950);

        // DropShadow effect
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.color(0.4, 0.4, 0.4));

        // Glow effect
        Glow glow = new Glow();
        glow.setLevel(0.8);

        // Reflection effect
        Reflection reflection = new Reflection();
        reflection.setFraction(0.7);

        // Applying effects
        text.setEffect(dropShadow);
        dropShadow.setInput(glow);
        glow.setInput(reflection);

        return text;
  }
}




