package com.source.Data;

import com.google.api.services.storage.model.Bucket.Lifecycle.Rule.Action;
import com.source.controller.FromNavigationApp;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Nextpage  {

     private FromNavigationApp app;
     private GridPane view;
     private TextField field7;
     
    public Nextpage(FromNavigationApp app) {

        this.app = app;
        initialize();
      }
    
      private void initialize() {

        view = new GridPane();
        view.setPadding(new Insets(1,1,1,1));
        Image img = new Image("pinksc.jpg");

        ImageView iv = new ImageView(img);
        iv.setFitHeight(1080);
        iv.setFitWidth(1920);

        Text text1 = new Text("LEADERBOARD");
        text1.setFont(new Font(35));
        text1.setFill(Color.BLACK);

        Text texthb1 = new Text("JAVA-DSA");
        texthb1.setFont(new Font(35));
        texthb1.setFill(Color.BLACK);

        Image imgJava = new Image("javaimg.png");

        ImageView ivJava = new ImageView(imgJava);
        ivJava.setFitHeight(80);
        ivJava.setFitWidth(80);

        ProgressBar leaderboardProgressBar1 = new ProgressBar(0.50);
        leaderboardProgressBar1.setMinWidth(200);
        leaderboardProgressBar1.setMinHeight(20);
        leaderboardProgressBar1
                        .setStyle("-fx-accent: darkgreen; -fx-border-color: black; -fx-border-style: dotted;");

        VBox vBox1 = new VBox(ivJava, texthb1, leaderboardProgressBar1);
        vBox1.setMinHeight(200);
        vBox1.setMaxWidth(450);
        vBox1.setStyle("-fx-background-color:WHITE;" + "-fx-background-radius:30;");
        VBox.setMargin(ivJava, new Insets(20, 10, 0, 10));
        VBox.setMargin(texthb1, new Insets(10, 10, 0, 10));
        VBox.setMargin(leaderboardProgressBar1, new Insets(10, 10, 10, 10));

        Text texthb2 = new Text("MATHEMATICS");
        texthb2.setFont(new Font(35));
        texthb2.setFill(Color.BLACK);

        Image imgMath = new Image("myMath.png");

        ImageView ivMath = new ImageView(imgMath);
        ivMath.setFitHeight(80);
        ivMath.setFitWidth(80);

        ProgressBar leaderboardProgressBar2 = new ProgressBar(0.50);
        leaderboardProgressBar2.setMinWidth(200);
        leaderboardProgressBar2.setMinHeight(20);
        leaderboardProgressBar2
                        .setStyle("-fx-accent: darkgreen; -fx-border-color: black; -fx-border-style: dotted;");

        VBox vBox2 = new VBox(ivMath, texthb2, leaderboardProgressBar2);
        vBox2.setMinHeight(200);
        vBox2.setMaxWidth(450);
        vBox2.setStyle("-fx-background-color:WHITE;" + "-fx-background-radius:30;");
        VBox.setMargin(ivMath, new Insets(20, 10, 0, 10));
        VBox.setMargin(texthb2, new Insets(10, 10, 0, 10));
        VBox.setMargin(leaderboardProgressBar2, new Insets(10, 10, 10, 10));

        Image imgChemistry = new Image("myMath.png");

        ImageView ivChemistry = new ImageView(imgChemistry);
        ivChemistry.setFitHeight(80);
        ivChemistry.setFitWidth(80);

        Text texthb3 = new Text("CHEMISTRY");
        texthb3.setFont(new Font(35));
        texthb3.setFill(Color.BLACK);

        ProgressBar leaderboardProgressBar3 = new ProgressBar(0.50);
        leaderboardProgressBar3.setMinWidth(200);
        leaderboardProgressBar3.setMinHeight(20);
        leaderboardProgressBar3
                        .setStyle("-fx-accent: darkgreen; -fx-border-color: black; -fx-border-style: dotted;");

        VBox vBox3 = new VBox(ivChemistry, texthb3, leaderboardProgressBar3);
        vBox3.setMinHeight(200);
        vBox3.setMaxWidth(450);
        vBox3.setStyle("-fx-background-color:WHITE;" + "-fx-background-radius:30;");
        VBox.setMargin(ivChemistry, new Insets(20, 10, 0, 10));
        VBox.setMargin(texthb3, new Insets(10, 10, 0, 10));
        VBox.setMargin(leaderboardProgressBar3, new Insets(10, 10, 10, 10));

        VBox vbox = new VBox(text1, vBox1, vBox2, vBox3);
        vbox.setMinSize(500, 1200);
        vbox.setLayoutX(00);
        vbox.setLayoutY(90);
        vbox.setStyle("-fx-background-color: lightgreen; -fx-background-square: 20;");
        VBox.setMargin(text1, new Insets(40, 120, 0, 120));
        VBox.setMargin(vBox1, new Insets(40, 50, 0, 50));
        VBox.setMargin(vBox2, new Insets(40, 50, 0, 50));
        VBox.setMargin(vBox3, new Insets(40, 50, 0, 50));

        Button bt2 = new Button("Home");
        bt2.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event){

                app.navigateToPage3();
            }
        });
        Button bt3 = new Button("Forum");
       // Button bt4 = new Button("Enquiry");
        Button bt5 = new Button("About Us");
        bt5.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event){

                app.navigateToPage7();
            }
        });

        // Styling the buttons to show only text with a good font
        String buttonStyle = "-fx-background-color: transparent; -fx-text-fill: white; -fx-font-size: 16pt; -fx-font-family: 'Arial'; -fx-font-weight:bold;";
        bt2.setStyle(buttonStyle);
        bt3.setStyle(buttonStyle);
       // bt4.setStyle(buttonStyle);
        bt5.setStyle(buttonStyle);

        Text textQuiz = createStyledText("Quizzo!", "Arial", FontWeight.BOLD, FontPosture.ITALIC, 50, 50, 60);

        Image imgLogo = new Image("quizlogo.png");

        ImageView ivLogo = new ImageView(imgLogo);
        ivLogo.setFitHeight(60);
        ivLogo.setFitWidth(60);

        HBox hb1 = new HBox(20, textQuiz, ivLogo); // Adding spacing between buttons
        hb1.setPadding(new Insets(20)); // Adding padding around the HBox
        hb1.setMinWidth(1920);
        hb1.setMinHeight(100);
        hb1.setLayoutX(0);
        hb1.setLayoutY(0);
        hb1.setStyle("-fx-background-color:BLACK;" + "-fx-background-square:30;");

        // Create a circular profile button
        Image imgProfile = new Image("kunals.jpeg");
        ImageView ivProfile = new ImageView(imgProfile);
        ivProfile.setFitHeight(70);
        ivProfile.setFitWidth(70);

        // Create a circle for clipping the profile iage
        Circle circleProfile = new Circle(40, 40, 30);
        ivProfile.setClip(circleProfile);

        Button profileButton = new Button();
        profileButton.setGraphic(ivProfile);
        profileButton.setStyle("-fx-background-color: transparent;");

        // Create a spacer to push the buttons to the right
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        hb1.getChildren().addAll(spacer, bt2, bt3,  bt5, profileButton);

        Image myImg = new Image("kunals.jpeg");

        ImageView iv1 = new ImageView(myImg);
        iv1.setFitHeight(200);
        iv1.setFitWidth(200);

        // Create a circle for clipping
        Circle circle = new Circle(100, 100, 100);
        iv1.setClip(circle);

        Text text2 = new Text("KUNAL SOMANATH SONAWANE");
        text2.setFont(new Font(30));
        text2.setFill(Color.WHITE);

        Image penimg = new Image("pen.jpg");

        ImageView ivPen = new ImageView(penimg);
        ivPen.setFitHeight(15);
        ivPen.setFitWidth(15);

        Button bt1 = new Button();
        bt1.setGraphic(ivPen);
        bt1.setMinHeight(5);
        bt1.setMinWidth(5);

        HBox hb2 = new HBox(20, iv1, text2, bt1); // Adding spacing between elements
        hb2.setMinWidth(1400);
        hb2.setMinHeight(350);
        hb2.setStyle("-fx-background-color:BLACK;" + "-fx-background-radius:30;");
        HBox.setMargin(iv1, new Insets(60, 50, 0, 50));
        HBox.setMargin(text2, new Insets(100, 50, 0, 0));
        HBox.setMargin(bt1, new Insets(100, 0, 0, 0));

        Text text3 = new Text("Profile Strength");
        text3.setFont(Font.font("Arial", FontWeight.BOLD, 35));
        text3.setFill(Color.LIGHTGREEN);

        ProgressBar progressBar = new ProgressBar(0.75);
        progressBar.setMinWidth(600);
        progressBar.setMinHeight(30);

        // Adding CSS styling directly to make it more attractive
        progressBar.setStyle("-fx-accent: lightblue; -fx-border-color: darkblue; -fx-border-style: dotted;");

        HBox hb3 = new HBox(20, text3, progressBar); // Adding spacing between elements
        hb3.setMinWidth(1200);
        hb3.setMinHeight(200);
        hb3.setStyle("-fx-background-color:BLACK;" + "-fx-background-radius:30;");
        HBox.setMargin(text3, new Insets(10, 60, 0, 20));
        HBox.setMargin(progressBar, new Insets(100, 1500, 0, 10));

        Text text4 = new Text("Achievement");
        text4.setFont(Font.font("Arial", FontWeight.BOLD, 35));
        text4.setFill(Color.LIGHTGREEN);

        HBox hb4 = new HBox(text4); // Adding spacing between elements
        hb4.setMinWidth(1200);
        hb4.setMinHeight(200);
        hb4.setStyle("-fx-background-color:BLACK;" + "-fx-background-radius:30;");
        hb4.setMargin(text4, new Insets(10, 50, 0, 20));

        Text text5 = new Text("Education");
        text5.setFont(Font.font("Arial", FontWeight.BOLD, 35));
        text5.setFill(Color.LIGHTGREEN);

        Text textE = new Text("(Master of Computer Application)-MODERN COLLEGE,PUNE");
        textE.setFont(new Font(20));
        textE.setFill(Color.WHITE);

        HBox hb5 = new HBox(text5, textE); // Adding spacing between elements
        hb5.setMinWidth(1200);
        hb5.setMinHeight(200);
        hb5.setStyle("-fx-background-color:BLACK;" + "-fx-background-radius:30;");
        hb5.setMargin(text5, new Insets(10, 50, 0, 20));
        hb5.setMargin(textE, new Insets(80, 50, 0, 20));

        Text text6 = new Text("About us");
        text6.setFont(Font.font("Arial", FontWeight.BOLD, 35));
        text6.setFill(Color.LIGHTGREEN);

        Text textab = new Text("Please add something about yourself");
        textab.setFont(new Font(20));
        textab.setFill(Color.WHITE);

        HBox hb6 = new HBox(text6, textab); // Adding spacing between elements
        hb6.setMinWidth(1200);
        hb6.setMinHeight(200);
        hb6.setStyle("-fx-background-color:BLACK;" + "-fx-background-radius:30;");
        hb6.setMargin(text6, new Insets(10, 50, 0, 20));
        hb6.setMargin(textab, new Insets(80, 50, 0, 20));

        Text text7 = new Text("Skills and Endorsements");
        text7.setFont(Font.font("Arial", FontWeight.BOLD, 35));
        text7.setFill(Color.LIGHTGREEN);

        HBox hb7 = new HBox(text7); // Adding spacing between elements
        hb7.setMinWidth(1200);
        hb7.setMinHeight(200);
        hb7.setStyle("-fx-background-color:BLACK;" + "-fx-background-radius:30;");
        hb7.setMargin(text7, new Insets(10, 50, 0, 20));

        Text text8 = new Text("Contact");
        text8.setFont(Font.font("Arial", FontWeight.BOLD, 35));
        text8.setFill(Color.LIGHTGREEN);

        Text textC = new Text("📱+91-9975352267,kunalsonawane5802@gmail.com");
        textC.setFont(new Font(20));
        textC.setFill(Color.WHITE);

        HBox hb8 = new HBox(text8, textC); // Adding spacing between elements
        hb8.setMinWidth(1200);
        hb8.setMinHeight(200);
        hb8.setStyle("-fx-background-color:BLACK;" + "-fx-background-radius:30;");
        hb8.setMargin(text8, new Insets(10, 50, 0, 20));
        hb8.setMargin(textC, new Insets(80, 50, 0, 20));

        VBox scrollContent = new VBox(20, hb2, hb3, hb4, hb5, hb6, hb7, hb8);
        scrollContent.setPadding(new Insets(20));

        ScrollPane scrollPane = new ScrollPane(scrollContent);
        scrollPane.setPrefWidth(1420);
        scrollPane.setPrefHeight(920);
        scrollPane.setLayoutX(500);
        scrollPane.setLayoutY(120);
        scrollPane.setStyle("-fx-background-color: lightblue;");

        Group gp = new Group(iv, vbox, hb1, scrollPane);

        view.add(gp,0,0);

        // Scene sc = new Scene(gp, 1920, 1080);
        // prStage.setScene(sc);
        // sc.setFill(Color.PINK);
        // sc.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        // prStage.show();
    }

    private Text createStyledText(String content, String fontFamily, FontWeight fontWeight, FontPosture fontPosture,
    double fontSize, double x, double y) {
Text text = new Text(content);
text.setFont(Font.font(fontFamily, fontWeight, fontPosture, fontSize));
text.setFill(Color.PURPLE);
text.setX(1500);
text.setY(950);

// DropShadow effect
DropShadow dropShadow = new DropShadow();
dropShadow.setOffsetX(3.0);
dropShadow.setOffsetY(3.0);
dropShadow.setColor(Color.color(0.4, 0.4, 0.4));

// Glow effect
Glow glow = new Glow();
glow.setLevel(0.8);

// Reflection effect
Reflection reflection = new Reflection();
reflection.setFraction(0.7);

// Applying effects
text.setEffect(dropShadow);
dropShadow.setInput(glow);
glow.setInput(reflection);

return text;
}
private Label createStyledLabel(String text, String fontFamily, FontWeight fontWeight, double fontSize) {
    Label label = new Label(text);
    label.setFont(Font.font(fontFamily, fontWeight, fontSize));
    label.setTextFill(Color.WHITE);
    label.setPadding(new Insets(20));
    label.setBackground(
                    new Background(new BackgroundFill(Color.PURPLE, new CornerRadii(10), Insets.EMPTY)));
    label.setBorder(new Border(
                    new BorderStroke(Color.WHITE, BorderStrokeStyle.SOLID, new CornerRadii(10),
                                    new BorderWidths(2))));
    label.setEffect(new DropShadow(10, Color.BLACK));
    return label;
}
    
    public GridPane getView() {

        return view;
      }

}
