package com.source.Data;

import com.google.api.services.storage.model.Bucket.Lifecycle.Rule.Action;
import com.source.controller.FromNavigationApp;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JavaQuizExample  {

    // Quiz questions and answers
    private final String[] questions = {
            "1)Which company developed Java?",
            "2)Which keyword is used to inherit a class in Java?",
            "3)Which method is the entry point of a Java program?",
            "4)Which of these is not a Java keyword?",
            "5)Which keyword is used to create an instance of a class?",
            "6)Which class is the parent class of all classes in Java?",
            "7)What is the size of an int variable in Java?",
            "8)Which exception is thrown when a null object is accessed?",
            "9)What is the default value of a boolean variable in Java?",
            "10)Which of these is a valid Java identifier?"
    };

    private final String[][] options = {
            {"Sun Microsystems", "Microsoft", "Google", "Apple"},
            {"extends", "implements", "inherits", "super"},
            {"main()", "start()", "run()", "init()"},
            {"final", "static", "include", "try"},
            {"new", "create", "make", "build"},
            {"Object", "Class", "Superclass", "Parent"},
            {"4 bytes", "2 bytes", "8 bytes", "16 bytes"},
            {"NullPointerException", "ArrayIndexOutOfBoundsException", "ClassCastException", "ArithmeticException"},
            {"true", "false", "null", "undefined"},
            {"1variable", "variable1", "1_variable", "variable-1"}
    };

    private final String[] correctAnswers = {
            "Sun Microsystems",
            "extends",
            "main()",
            "include",
            "new",
            "Object",
            "4 bytes",
            "NullPointerException",
            "false",
            "variable1"
    };

    private final String[] explanations = {
            "Java was originally developed by James Gosling at Sun Microsystems (which has since been acquired by Oracle).",
            "The 'extends' keyword is used to inherit a class in Java.",
            "The main() method is the entry point of any Java program.",
            "'include' is not a keyword in Java; it's used in C/C++.",
            "The 'new' keyword is used to create new instances of objects.",
            "The Object class is the parent class of all classes in Java.",
            "An int variable in Java is 4 bytes (32 bits).",
            "A NullPointerException is thrown when a program attempts to use an object reference that has the null value.",
            "The default value of a boolean variable in Java is false.",
            "A valid Java identifier can start with a letter, dollar sign ($), or underscore (_)."
    };

    private int currentQuestionIndex = 0;
    private int score = 0;
    private Label questionLabel;
    private Label resultLabel;
    private Label explanationLabel;
    private Label scoreLabel;
    private RadioButton[] optionButtons;
    private ToggleGroup group;
    private Button nextButton;
    private Button prevButton;

    // Store user selections and results for each question
    private final String[] userSelections = new String[questions.length];
    private final String[] userResults = new String[questions.length];
    private final boolean[] answeredCorrectly = new boolean[questions.length];

   private FromNavigationApp app;
     private GridPane view;
     private TextField field9=new TextField();
     
    public JavaQuizExample(FromNavigationApp app) {

        this.app = app;
        initialize();
      }
    
      private void initialize() {

        view = new GridPane();
        view.setPadding(new Insets(1,1,1,1));

        questionLabel = new Label();
        questionLabel.setStyle("-fx-font-size: 24px; -fx-padding: 10px;");
        
        resultLabel = new Label();
        resultLabel.setLayoutX(650);
        resultLabel.setLayoutY(520);
        
        explanationLabel = new Label();
        explanationLabel.setLayoutX(640);
        explanationLabel.setLayoutY(550);
        explanationLabel.setStyle("-fx-font-size: 20px;");
        scoreLabel = new Label("Score: 0");
        scoreLabel.setLayoutX(550);
        scoreLabel.setLayoutY(50);
        scoreLabel.setStyle("-fx-font-size: 24px;-fx-background-color:black;-fx-background-radius:20;-fx-text-fill:white;-fx-padding: 6px");
        
        optionButtons = new RadioButton[4];
        
        group = new ToggleGroup();

        for (int i = 0; i < 4; i++) {
            optionButtons[i] = new RadioButton();
            optionButtons[i].setToggleGroup(group);
            optionButtons[i].setStyle("-fx-font-size: 18px; -fx-background-color: none; -fx-padding: 10px;");
        }

        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                RadioButton selectedRadioButton = (RadioButton) newValue;
                checkAnswer(selectedRadioButton.getText());
                disableRadioButtons();
            }
        });

        nextButton = new Button("Next");
        nextButton.setLayoutX(1300);
        nextButton.setLayoutY(450);
        nextButton.setStyle("-fx-font-size: 18px;-fx-background-color:#00008B;-fx-background-radius:20;-fx-text-fill:white;");
        nextButton.setOnAction(e -> nextQuestion());

        prevButton = new Button("Previous");
        prevButton.setLayoutX(570);
        prevButton.setLayoutY(450);
        prevButton.setStyle("-fx-font-size: 18px;-fx-background-color:#00008B;-fx-background-radius:20;-fx-text-fill:white;");
        prevButton.setOnAction(e -> prevQuestion());

        HBox hb = new HBox(10, questionLabel);
        hb.setPadding(new Insets(10));
        hb.setLayoutX(550);
        hb.setLayoutY(100);
        hb.setMinSize(900, 60);
        hb.setStyle("-fx-background-color: wheat; -fx-background-radius: 20;");

        VBox hb1 = new VBox(10, optionButtons[0], optionButtons[1], optionButtons[2], optionButtons[3]);
        hb1.setPadding(new Insets(10));
        hb1.setLayoutX(550);
        hb1.setLayoutY(190);
        hb1.setMinSize(900, 200);
        hb1.setStyle("-fx-background-color: wheat; -fx-background-radius: 20;");

        // Image img2 = new Image("qui.jpg");
        // ImageView imgg = new ImageView(img2);
        // HBox hbb = new HBox(imgg);
        // hbb.setMinHeight(400);
        // hbb.setMinWidth(400);
        // hbb.setLayoutX(1500);
        // hbb.setLayoutY(100);

        Button back = new Button("Back");
        back.setStyle("-fx-background-color: linear-gradient(#25c52d, #62e09d);-fx-text-fill: white;-fx-font-size: 20px;-fx-font-weight: bold;");
        back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
                app.navigateToPage4();
            }
        });
        back.setLayoutX(0);
        back.setLayoutY(0);
        Group gp = new Group(hb, hb1,resultLabel, explanationLabel, scoreLabel,prevButton, nextButton,back);

        view.add(gp,0,0);
        // Scene scene = new Scene(gp, 1920, 1080);
        // primaryStage.setScene(scene);
        // primaryStage.setTitle("JavaFX Java Quiz Example");
        // primaryStage.show();

        showQuestion(currentQuestionIndex);
    }

    private void showQuestion(int index) {
        if (index < questions.length) {
            questionLabel.setText(questions[index]);
            for (int i = 0; i < 4; i++) {
                optionButtons[i].setText(options[index][i]);
                optionButtons[i].setDisable(false); // Enable radio buttons
                optionButtons[i].setStyle("-fx-font-size: 18px; -fx-background-color: none; -fx-padding: 10px;"); // Reset background color
            }
            resultLabel.setText("");
            explanationLabel.setText("");
            group.selectToggle(null); // Deselect any selected option

            // Restore previous selection and result
            if (userSelections[index] != null) {
                for (RadioButton optionButton : optionButtons) {
                    if (optionButton.getText().equals(userSelections[index])) {
                        optionButton.setSelected(true);
                        resultLabel.setText(userResults[index]);
                        setOptionButtonStyle(userSelections[index], userResults[index].equals("Correct!") ? "green" : "red");
                        if (!userResults[index].equals("Correct!")) {
                            setOptionButtonStyle(correctAnswers[index], "green");
                        }
                        explanationLabel.setText(explanations[index]);
                        disableRadioButtons();
                        break;
                    }
                }
            }

            prevButton.setDisable(currentQuestionIndex == 0); // Disable the previous button if on the first question
            nextButton.setDisable(currentQuestionIndex == questions.length - 1); // Disable the next button if on the last question
        } else {
            // Quiz completed
            showCompletionAlert();
        }
    }

    private void checkAnswer(String selectedAnswer) {
        if (selectedAnswer.equals(correctAnswers[currentQuestionIndex])) {
            resultLabel.setText("Correct!");
            resultLabel.setStyle("-fx-text-fill:green;-fx-font-size:20px;");
            if (!answeredCorrectly[currentQuestionIndex]) {
                score++;
                answeredCorrectly[currentQuestionIndex] = true;
            }
            scoreLabel.setText("Score: " + score);
            setOptionButtonStyle(selectedAnswer, "green");
        } else {
            resultLabel.setText("Incorrect. The correct answer is " + correctAnswers[currentQuestionIndex] + ".");
            resultLabel.setStyle("-fx-text-fill:Red;-fx-font-size:20px;");
            if (answeredCorrectly[currentQuestionIndex]) {
                score--;
                answeredCorrectly[currentQuestionIndex] = false;
            }
            setOptionButtonStyle(selectedAnswer, "red");
            setOptionButtonStyle(correctAnswers[currentQuestionIndex], "green");
        }
        explanationLabel.setText(explanations[currentQuestionIndex]);

        // Store user selection and result
        userSelections[currentQuestionIndex] = selectedAnswer;
        userResults[currentQuestionIndex] = resultLabel.getText();

        // Automatically show completion alert if this is the last question
        if (currentQuestionIndex == questions.length - 1) {
            showCompletionAlert();
        }
    }

    private void setOptionButtonStyle(String answer, String color) {
        for (RadioButton optionButton : optionButtons) {
            if (optionButton.getText().equals(answer)) {
                optionButton.setStyle("-fx-font-size: 18px; -fx-background-color: " + color + "; -fx-padding: 10px; -fx-background-radius: 20;");
            }
        }
    }

    private void disableRadioButtons() {
        for (RadioButton optionButton : optionButtons) {
            optionButton.setDisable(true);
        }
    }

    private void nextQuestion() {
        if (currentQuestionIndex < questions.length - 1) {
            currentQuestionIndex++;
            showQuestion(currentQuestionIndex);
        } else {
            showCompletionAlert();
        }
    }

    private void prevQuestion() {
        if (currentQuestionIndex > 0) {
            currentQuestionIndex--;
            showQuestion(currentQuestionIndex);
        }
    }

    // private void showCompletionAlert() {
    //     Alert alert = new Alert(Alert.AlertType.INFORMATION);
    //     alert.setTitle("Quiz Completed");
    //     alert.setHeaderText("Quiz completed!");
    //     alert.setContentText("Your final score is " + score + "/" + questions.length + ".");
    //     alert.getDialogPane().setStyle("-fx-font-size: 16px; -fx-background-radius: 20;");
    //     alert.showAndWait();
    // }

    private void showCompletionAlert() {
    Stage dialogStage = new Stage();
    dialogStage.setTitle("Quiz Completed");

    VBox vbox = new VBox(20); // 20 is the spacing between elements
    vbox.setAlignment(Pos.CENTER);
    vbox.setPadding(new Insets(20));
    vbox.setStyle("-fx-background-color:white; -fx-background-radius: 00;");

    Label headerLabel = new Label("Quiz completed! 🎊");
    headerLabel.setStyle("-fx-font-size: 20px; -fx-font-weight: bold; -fx-text-fill: #333;");

    Label contentLabel = new Label("Your final score is " + score + "/" + questions.length + ".");
    contentLabel.setStyle("-fx-font-size: 16px; -fx-text-fill: green;");

    Button okButton = new Button("OK");
    okButton.setStyle("-fx-font-size: 18px; -fx-background-color: #00008B; -fx-background-radius: 20; -fx-text-fill: white;");
    okButton.setOnAction(e -> dialogStage.close());

    vbox.getChildren().addAll(headerLabel, contentLabel, okButton);
    vbox.setAlignment(Pos.CENTER); // Center align elements

    Scene scene = new Scene(vbox);
    dialogStage.setScene(scene);
    dialogStage.setResizable(false);
    dialogStage.showAndWait();
}

    public GridPane getView() {

        return view;
      }
}
