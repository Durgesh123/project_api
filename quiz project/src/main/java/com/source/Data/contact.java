package com.source.Data;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import com.source.controller.FromNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;

import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class contact {

    private FromNavigationApp app;
    private GridPane view;
    private TextField field8;
    
    public contact(FromNavigationApp app){

        this.app = app;
        initialize();
    }

    private void initialize(){

        view = new GridPane();
        view.setPadding(new Insets(1,1,1,1));
        view.setStyle("-fx-background-color: #90EE90;");

        Button home = new Button("Home");
        home.setStyle("-fx-font-size: 25px;-fx-background-color: transparent;-fx-text-fill: black;  -fx-border-radius: 15; -fx-background-radius: 15; -fx-font-weight: bold;");
        home.setLayoutX(1620);
        home.setLayoutY(50);
        home.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
              app.navigateToPage1();
            }
          });
  

        // Contact Button
        Button aboutus = new Button("AboutUs");
        aboutus.setStyle("-fx-font-size: 25px;-fx-background-color: transparent;-fx-text-fill: black;  -fx-border-radius: 15; -fx-background-radius: 15; -fx-font-weight: bold;");
        aboutus.setLayoutX(1750);
        aboutus.setLayoutY(50);
        aboutus.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
              app.navigateToPage7();
            }
          });
  

        Label lb = new Label("CONTACT US");
        lb.setStyle("-fx-font-size:30;-fx-font-weight: bold;");
        lb.setLayoutX(50);
        lb.setLayoutY(60);

        String n;
        Label ds = new Label("We would love to hear from you!Whether you have questions, feedback, or need assistance, our team is here to help. \nPlease reach out to us through any of the following methods:");
        ds.setStyle("-fx-font-size:20;");
        ds.setLayoutX(50);
        ds.setLayoutY(100);
 
        Image img = new Image("contact.jpg");
        ImageView imgv = new ImageView(img);
        imgv.setFitHeight(700);
        imgv.setFitWidth(700);

        VBox vb = new VBox(imgv);
        vb.setMinSize(500, 500);
        vb.setLayoutX(950);
        vb.setLayoutY(160);

        Image email = new Image("email.png");
        ImageView emaiv = new ImageView(email);
        emaiv.setFitHeight(50);
        emaiv.setFitWidth(50);
        emaiv.setLayoutX(70);
        emaiv.setLayoutY(180);

        Label lb1 = new Label("durgesh@gmail.com");
        lb1.setStyle("-fx-font-size:20;-fx-font-weight:bold;");
        lb1.setLayoutX(130);
        lb1.setLayoutY(185);

        
        Image tele = new Image("phone.png");
        ImageView telev = new ImageView(tele);
        telev.setFitHeight(45);
        telev.setFitWidth(45);
        telev.setLayoutX(70);
        telev.setLayoutY(240);

        Label lb2 = new Label("8788862365");
        lb2.setStyle("-fx-font-size:20;-fx-font-weight:bold;");
        lb2.setLayoutX(130);
        lb2.setLayoutY(250);

        Label Name = new Label("Name :");
        Name.setStyle("-fx-font-weight:bold;");

        Label Email = new Label("Email :");
        Email.setStyle("-fx-font-weight:bold;");

        Label About = new Label("About :");
        About.setStyle("-fx-font-weight:bold;");

        TextField name = new TextField();
        name.setMaxWidth(480);
        name.setStyle("-fx-border-color: black; -fx-border-width: 2px;");

        TextField emailf = new TextField();
        emailf.setMaxWidth(480);
        emailf.setStyle("-fx-border-color: black; -fx-border-width: 2px;");

        TextField about = new TextField();
        about.setPrefWidth(300); // Set your desired width
        about.setPrefHeight(100);
        about.setStyle("-fx-border-color: black; -fx-border-width: 2px;");

        Button bt = new Button("Submit");
        bt.setLayoutX(450);
        bt.setLayoutY(580);

        bt.setStyle("-fx-background-color: black; -fx-text-fill: white; -fx-font-size: 12px; -fx-min-width: 90px; -fx-min-height: 30px; -fx-background-radius: 20;");

        Image facea = new Image("face.png");
        ImageView facev = new ImageView(facea);
        facev.setFitHeight(45);
        facev.setFitWidth(45);
        facev.setLayoutX(70);
        facev.setLayoutY(600);

        
        Image insta = new Image("insta.png");
        ImageView instav = new ImageView(insta);
        instav.setFitHeight(45);
        instav.setFitWidth(45);
        instav.setLayoutX(150);
        instav.setLayoutY(600);

        Image twitt = new Image("twitter.png");
        ImageView twittv = new ImageView(twitt);
        twittv.setFitHeight(45);
        twittv.setFitWidth(45);
        twittv.setLayoutX(230);
        twittv.setLayoutY(600);
        

        VBox vb1 = new VBox(10,Name,name,Email,emailf,About,about);
        vb1.setMinSize(500, 300);
        vb1.setStyle("-fx-background-color:white;-fx-padding:10px");
        vb1.setLayoutX(50);
        vb1.setLayoutY(300);

       
        Group gp = new Group(lb,ds,vb,emaiv,lb1,telev,lb2,vb1,bt,facev,instav,twittv,home,aboutus);
     
        view.add(gp, 0 ,0);
        // Scene sc = new Scene(gp,1920,1080);
        // sc.setFill(Color.BURLYWOOD);
        // prstage.setScene(sc);
        // prstage.show();


    }

    public GridPane getView(){

        return view;
    }
  
    public String getField8Value(){
  
        return field8.getText();
    }
  
    public void setField8Value(String value){
  
        field8.setText(value);
    }
  
    
}