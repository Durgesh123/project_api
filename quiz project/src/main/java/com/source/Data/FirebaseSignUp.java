package com.source.Data;


import java.io.FileInputStream;
import java.io.IOException;

//import com.firebase.controller.loginPage;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.util.concurrent.ExecutionError;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FirebaseSignUp extends Application {

    private Stage primaryStage;
    private loginController signupController;
    private Scene scene;

    public void setPrimaryStageScene(Scene scene){

        primaryStage.setScene(scene);
    }
    public void initializeLoginScene(){

        Scene loginScene = createLoginScene();
        this.scene = loginScene;
        primaryStage.setScene(loginScene);
    }
    @Override
    public void start(Stage primaryStage){

        this.primaryStage = primaryStage;

        try{
                FileInputStream serviceAccount = new FileInputStream("src/main/resources/fx-auth-fb.json");

                FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://java-auth-5e494-default-rtdb.firebaseio.com")
                .build();
            
            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Scene scene = createLoginScene();
        this.scene = scene;
        primaryStage.setTitle("Sign Up Page");
        primaryStage.setScene(scene);
        primaryStage.show();

        }
        private Scene createLoginScene(){

            TextField emailField = new TextField();
            emailField.setPromptText("Email");

            PasswordField passwordField = new PasswordField();
            passwordField.setPromptText("password");

            Button signUpButton = new Button("SignUp");
            Button loginButton = new Button("Login");

         //   signupController = new loginController (this,emailField,passwordField);

            signUpButton.setOnAction(new EventHandler<ActionEvent>() {
                
                @Override
                public void handle(ActionEvent event){
                    signupController.signUp();
                }
            });
            loginButton.setOnAction(new EventHandler<ActionEvent>() {
            
                @Override
                public void handle(ActionEvent event){
                    signupController.login();
                }
            });

        VBox fieldBox = new VBox(20, emailField, passwordField);
        HBox buttonBox = new HBox(20, loginButton, signUpButton);
        VBox conBox = new VBox(10, buttonBox,fieldBox);
        Pane viewPane = new Pane(conBox);
        viewPane.setStyle("-fx-background-color:#508BFC;-fx-background-radius: 20;");
        return new Scene(viewPane, 1920, 1080);
            

        }
  }
    

